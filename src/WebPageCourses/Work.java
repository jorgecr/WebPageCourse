/**
 */
package WebPageCourses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Work</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.Work#getPorcentage <em>Porcentage</em>}</li>
 *   <li>{@link WebPageCourses.Work#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see WebPageCourses.WebPageCoursesPackage#getWork()
 * @model
 * @generated
 */
public interface Work extends EObject {
	/**
	 * Returns the value of the '<em><b>Porcentage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Porcentage</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Porcentage</em>' attribute.
	 * @see #setPorcentage(double)
	 * @see WebPageCourses.WebPageCoursesPackage#getWork_Porcentage()
	 * @model
	 * @generated
	 */
	double getPorcentage();

	/**
	 * Sets the value of the '{@link WebPageCourses.Work#getPorcentage <em>Porcentage</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Porcentage</em>' attribute.
	 * @see #getPorcentage()
	 * @generated
	 */
	void setPorcentage(double value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see WebPageCourses.WebPageCoursesPackage#getWork_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link WebPageCourses.Work#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Work
