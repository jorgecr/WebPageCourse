/**
 */
package WebPageCourses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Coursework</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.Coursework#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link WebPageCourses.Coursework#getLabHours <em>Lab Hours</em>}</li>
 * </ul>
 *
 * @see WebPageCourses.WebPageCoursesPackage#getCoursework()
 * @model
 * @generated
 */
public interface Coursework extends EObject {
	/**
	 * Returns the value of the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Hours</em>' attribute.
	 * @see #setLectureHours(double)
	 * @see WebPageCourses.WebPageCoursesPackage#getCoursework_LectureHours()
	 * @model
	 * @generated
	 */
	double getLectureHours();

	/**
	 * Sets the value of the '{@link WebPageCourses.Coursework#getLectureHours <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecture Hours</em>' attribute.
	 * @see #getLectureHours()
	 * @generated
	 */
	void setLectureHours(double value);

	/**
	 * Returns the value of the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lab Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lab Hours</em>' attribute.
	 * @see #setLabHours(double)
	 * @see WebPageCourses.WebPageCoursesPackage#getCoursework_LabHours()
	 * @model
	 * @generated
	 */
	double getLabHours();

	/**
	 * Sets the value of the '{@link WebPageCourses.Coursework#getLabHours <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lab Hours</em>' attribute.
	 * @see #getLabHours()
	 * @generated
	 */
	void setLabHours(double value);

} // Coursework
