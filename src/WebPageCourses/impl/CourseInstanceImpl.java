/**
 */
package WebPageCourses.impl;

import WebPageCourses.CourseInstance;
import WebPageCourses.EvaluationForm;
import WebPageCourses.Organization;
import WebPageCourses.Timetable;
import WebPageCourses.WebPageCoursesPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.impl.CourseInstanceImpl#getSemester <em>Semester</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseInstanceImpl#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseInstanceImpl#getOrganizations <em>Organizations</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseInstanceImpl#getTimetable <em>Timetable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends MinimalEObjectImpl.Container implements CourseInstance {
	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final String SEMESTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected String semester = SEMESTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEvaluationform() <em>Evaluationform</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationform()
	 * @generated
	 * @ordered
	 */
	protected EvaluationForm evaluationform;

	/**
	 * The cached value of the '{@link #getOrganizations() <em>Organizations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrganizations()
	 * @generated
	 * @ordered
	 */
	protected EList<Organization> organizations;

	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected Timetable timetable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageCoursesPackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemester(String newSemester) {
		String oldSemester = semester;
		semester = newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE_INSTANCE__SEMESTER, oldSemester, semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationForm getEvaluationform() {
		return evaluationform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvaluationform(EvaluationForm newEvaluationform, NotificationChain msgs) {
		EvaluationForm oldEvaluationform = evaluationform;
		evaluationform = newEvaluationform;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM, oldEvaluationform, newEvaluationform);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluationform(EvaluationForm newEvaluationform) {
		if (newEvaluationform != evaluationform) {
			NotificationChain msgs = null;
			if (evaluationform != null)
				msgs = ((InternalEObject)evaluationform).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM, null, msgs);
			if (newEvaluationform != null)
				msgs = ((InternalEObject)newEvaluationform).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM, null, msgs);
			msgs = basicSetEvaluationform(newEvaluationform, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM, newEvaluationform, newEvaluationform));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Organization> getOrganizations() {
		if (organizations == null) {
			organizations = new EObjectContainmentEList<Organization>(Organization.class, this, WebPageCoursesPackage.COURSE_INSTANCE__ORGANIZATIONS);
		}
		return organizations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timetable getTimetable() {
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimetable(Timetable newTimetable, NotificationChain msgs) {
		Timetable oldTimetable = timetable;
		timetable = newTimetable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE, oldTimetable, newTimetable);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimetable(Timetable newTimetable) {
		if (newTimetable != timetable) {
			NotificationChain msgs = null;
			if (timetable != null)
				msgs = ((InternalEObject)timetable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE, null, msgs);
			if (newTimetable != null)
				msgs = ((InternalEObject)newTimetable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE, null, msgs);
			msgs = basicSetTimetable(newTimetable, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE, newTimetable, newTimetable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM:
				return basicSetEvaluationform(null, msgs);
			case WebPageCoursesPackage.COURSE_INSTANCE__ORGANIZATIONS:
				return ((InternalEList<?>)getOrganizations()).basicRemove(otherEnd, msgs);
			case WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE:
				return basicSetTimetable(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE_INSTANCE__SEMESTER:
				return getSemester();
			case WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM:
				return getEvaluationform();
			case WebPageCoursesPackage.COURSE_INSTANCE__ORGANIZATIONS:
				return getOrganizations();
			case WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE:
				return getTimetable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE_INSTANCE__SEMESTER:
				setSemester((String)newValue);
				return;
			case WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM:
				setEvaluationform((EvaluationForm)newValue);
				return;
			case WebPageCoursesPackage.COURSE_INSTANCE__ORGANIZATIONS:
				getOrganizations().clear();
				getOrganizations().addAll((Collection<? extends Organization>)newValue);
				return;
			case WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE:
				setTimetable((Timetable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE_INSTANCE__SEMESTER:
				setSemester(SEMESTER_EDEFAULT);
				return;
			case WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM:
				setEvaluationform((EvaluationForm)null);
				return;
			case WebPageCoursesPackage.COURSE_INSTANCE__ORGANIZATIONS:
				getOrganizations().clear();
				return;
			case WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE:
				setTimetable((Timetable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSE_INSTANCE__SEMESTER:
				return SEMESTER_EDEFAULT == null ? semester != null : !SEMESTER_EDEFAULT.equals(semester);
			case WebPageCoursesPackage.COURSE_INSTANCE__EVALUATIONFORM:
				return evaluationform != null;
			case WebPageCoursesPackage.COURSE_INSTANCE__ORGANIZATIONS:
				return organizations != null && !organizations.isEmpty();
			case WebPageCoursesPackage.COURSE_INSTANCE__TIMETABLE:
				return timetable != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (semester: ");
		result.append(semester);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl
