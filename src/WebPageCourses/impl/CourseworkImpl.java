/**
 */
package WebPageCourses.impl;

import WebPageCourses.Coursework;
import WebPageCourses.WebPageCoursesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Coursework</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.impl.CourseworkImpl#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link WebPageCourses.impl.CourseworkImpl#getLabHours <em>Lab Hours</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseworkImpl extends MinimalEObjectImpl.Container implements Coursework {
	/**
	 * The default value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected static final double LECTURE_HOURS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected double lectureHours = LECTURE_HOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected static final double LAB_HOURS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected double labHours = LAB_HOURS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageCoursesPackage.Literals.COURSEWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLectureHours() {
		return lectureHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLectureHours(double newLectureHours) {
		double oldLectureHours = lectureHours;
		lectureHours = newLectureHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSEWORK__LECTURE_HOURS, oldLectureHours, lectureHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLabHours() {
		return labHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabHours(double newLabHours) {
		double oldLabHours = labHours;
		labHours = newLabHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.COURSEWORK__LAB_HOURS, oldLabHours, labHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSEWORK__LECTURE_HOURS:
				return getLectureHours();
			case WebPageCoursesPackage.COURSEWORK__LAB_HOURS:
				return getLabHours();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSEWORK__LECTURE_HOURS:
				setLectureHours((Double)newValue);
				return;
			case WebPageCoursesPackage.COURSEWORK__LAB_HOURS:
				setLabHours((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSEWORK__LECTURE_HOURS:
				setLectureHours(LECTURE_HOURS_EDEFAULT);
				return;
			case WebPageCoursesPackage.COURSEWORK__LAB_HOURS:
				setLabHours(LAB_HOURS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.COURSEWORK__LECTURE_HOURS:
				return lectureHours != LECTURE_HOURS_EDEFAULT;
			case WebPageCoursesPackage.COURSEWORK__LAB_HOURS:
				return labHours != LAB_HOURS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lectureHours: ");
		result.append(lectureHours);
		result.append(", labHours: ");
		result.append(labHours);
		result.append(')');
		return result.toString();
	}

} //CourseworkImpl
