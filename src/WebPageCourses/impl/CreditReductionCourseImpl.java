/**
 */
package WebPageCourses.impl;

import WebPageCourses.CreditReductionCourse;
import WebPageCourses.WebPageCoursesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Credit Reduction Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.impl.CreditReductionCourseImpl#getCode <em>Code</em>}</li>
 *   <li>{@link WebPageCourses.impl.CreditReductionCourseImpl#getCreditReduction <em>Credit Reduction</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CreditReductionCourseImpl extends MinimalEObjectImpl.Container implements CreditReductionCourse {
	/**
	 * The default value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCode() <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCode()
	 * @generated
	 * @ordered
	 */
	protected String code = CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCreditReduction() <em>Credit Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditReduction()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDIT_REDUCTION_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCreditReduction() <em>Credit Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditReduction()
	 * @generated
	 * @ordered
	 */
	protected double creditReduction = CREDIT_REDUCTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CreditReductionCourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageCoursesPackage.Literals.CREDIT_REDUCTION_COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCode(String newCode) {
		String oldCode = code;
		code = newCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CODE, oldCode, code));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCreditReduction() {
		return creditReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreditReduction(double newCreditReduction) {
		double oldCreditReduction = creditReduction;
		creditReduction = newCreditReduction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CREDIT_REDUCTION, oldCreditReduction, creditReduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CODE:
				return getCode();
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CREDIT_REDUCTION:
				return getCreditReduction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CODE:
				setCode((String)newValue);
				return;
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CREDIT_REDUCTION:
				setCreditReduction((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CODE:
				setCode(CODE_EDEFAULT);
				return;
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CREDIT_REDUCTION:
				setCreditReduction(CREDIT_REDUCTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CODE:
				return CODE_EDEFAULT == null ? code != null : !CODE_EDEFAULT.equals(code);
			case WebPageCoursesPackage.CREDIT_REDUCTION_COURSE__CREDIT_REDUCTION:
				return creditReduction != CREDIT_REDUCTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (code: ");
		result.append(code);
		result.append(", creditReduction: ");
		result.append(creditReduction);
		result.append(')');
		return result.toString();
	}

} //CreditReductionCourseImpl
