/**
 */
package WebPageCourses.impl;

import WebPageCourses.Schedule;
import WebPageCourses.Timetable;
import WebPageCourses.WebPageCoursesPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timetable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.impl.TimetableImpl#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link WebPageCourses.impl.TimetableImpl#getLabHours <em>Lab Hours</em>}</li>
 *   <li>{@link WebPageCourses.impl.TimetableImpl#getSchedule <em>Schedule</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimetableImpl extends MinimalEObjectImpl.Container implements Timetable {
	/**
	 * The default value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected static final double LECTURE_HOURS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected double lectureHours = LECTURE_HOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected static final double LAB_HOURS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLabHours() <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected double labHours = LAB_HOURS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSchedule() <em>Schedule</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedule()
	 * @generated
	 * @ordered
	 */
	protected EList<Schedule> schedule;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimetableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebPageCoursesPackage.Literals.TIMETABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLectureHours() {
		return lectureHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLectureHours(double newLectureHours) {
		double oldLectureHours = lectureHours;
		lectureHours = newLectureHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.TIMETABLE__LECTURE_HOURS, oldLectureHours, lectureHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLabHours() {
		return labHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabHours(double newLabHours) {
		double oldLabHours = labHours;
		labHours = newLabHours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebPageCoursesPackage.TIMETABLE__LAB_HOURS, oldLabHours, labHours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Schedule> getSchedule() {
		if (schedule == null) {
			schedule = new EObjectContainmentEList<Schedule>(Schedule.class, this, WebPageCoursesPackage.TIMETABLE__SCHEDULE);
		}
		return schedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebPageCoursesPackage.TIMETABLE__SCHEDULE:
				return ((InternalEList<?>)getSchedule()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebPageCoursesPackage.TIMETABLE__LECTURE_HOURS:
				return getLectureHours();
			case WebPageCoursesPackage.TIMETABLE__LAB_HOURS:
				return getLabHours();
			case WebPageCoursesPackage.TIMETABLE__SCHEDULE:
				return getSchedule();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebPageCoursesPackage.TIMETABLE__LECTURE_HOURS:
				setLectureHours((Double)newValue);
				return;
			case WebPageCoursesPackage.TIMETABLE__LAB_HOURS:
				setLabHours((Double)newValue);
				return;
			case WebPageCoursesPackage.TIMETABLE__SCHEDULE:
				getSchedule().clear();
				getSchedule().addAll((Collection<? extends Schedule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.TIMETABLE__LECTURE_HOURS:
				setLectureHours(LECTURE_HOURS_EDEFAULT);
				return;
			case WebPageCoursesPackage.TIMETABLE__LAB_HOURS:
				setLabHours(LAB_HOURS_EDEFAULT);
				return;
			case WebPageCoursesPackage.TIMETABLE__SCHEDULE:
				getSchedule().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebPageCoursesPackage.TIMETABLE__LECTURE_HOURS:
				return lectureHours != LECTURE_HOURS_EDEFAULT;
			case WebPageCoursesPackage.TIMETABLE__LAB_HOURS:
				return labHours != LAB_HOURS_EDEFAULT;
			case WebPageCoursesPackage.TIMETABLE__SCHEDULE:
				return schedule != null && !schedule.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lectureHours: ");
		result.append(lectureHours);
		result.append(", labHours: ");
		result.append(labHours);
		result.append(')');
		return result.toString();
	}

} //TimetableImpl
