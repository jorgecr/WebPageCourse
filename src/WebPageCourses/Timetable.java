/**
 */
package WebPageCourses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timetable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.Timetable#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link WebPageCourses.Timetable#getLabHours <em>Lab Hours</em>}</li>
 *   <li>{@link WebPageCourses.Timetable#getSchedule <em>Schedule</em>}</li>
 * </ul>
 *
 * @see WebPageCourses.WebPageCoursesPackage#getTimetable()
 * @model
 * @generated
 */
public interface Timetable extends EObject {
	/**
	 * Returns the value of the '<em><b>Lecture Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecture Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecture Hours</em>' attribute.
	 * @see #setLectureHours(double)
	 * @see WebPageCourses.WebPageCoursesPackage#getTimetable_LectureHours()
	 * @model
	 * @generated
	 */
	double getLectureHours();

	/**
	 * Sets the value of the '{@link WebPageCourses.Timetable#getLectureHours <em>Lecture Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecture Hours</em>' attribute.
	 * @see #getLectureHours()
	 * @generated
	 */
	void setLectureHours(double value);

	/**
	 * Returns the value of the '<em><b>Lab Hours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lab Hours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lab Hours</em>' attribute.
	 * @see #setLabHours(double)
	 * @see WebPageCourses.WebPageCoursesPackage#getTimetable_LabHours()
	 * @model
	 * @generated
	 */
	double getLabHours();

	/**
	 * Sets the value of the '{@link WebPageCourses.Timetable#getLabHours <em>Lab Hours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lab Hours</em>' attribute.
	 * @see #getLabHours()
	 * @generated
	 */
	void setLabHours(double value);

	/**
	 * Returns the value of the '<em><b>Schedule</b></em>' containment reference list.
	 * The list contents are of type {@link WebPageCourses.Schedule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedule</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedule</em>' containment reference list.
	 * @see WebPageCourses.WebPageCoursesPackage#getTimetable_Schedule()
	 * @model containment="true"
	 * @generated
	 */
	EList<Schedule> getSchedule();

} // Timetable
