/**
 */
package WebPageCourses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluation Form</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.EvaluationForm#getWork <em>Work</em>}</li>
 * </ul>
 *
 * @see WebPageCourses.WebPageCoursesPackage#getEvaluationForm()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore/OCL finalGrade='Sequence{self.work.porcentage * self.work.grade} in c-&gt;sum() &lt;= self.finalGrade'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='finalGrade'"
 * @generated
 */
public interface EvaluationForm extends EObject {
	/**
	 * Returns the value of the '<em><b>Work</b></em>' containment reference list.
	 * The list contents are of type {@link WebPageCourses.Work}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Work</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Work</em>' containment reference list.
	 * @see WebPageCourses.WebPageCoursesPackage#getEvaluationForm_Work()
	 * @model containment="true"
	 * @generated
	 */
	EList<Work> getWork();

} // EvaluationForm
