/**
 */
package WebPageCourses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Credit Reduction Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link WebPageCourses.CreditReductionCourse#getCode <em>Code</em>}</li>
 *   <li>{@link WebPageCourses.CreditReductionCourse#getCreditReduction <em>Credit Reduction</em>}</li>
 * </ul>
 *
 * @see WebPageCourses.WebPageCoursesPackage#getCreditReductionCourse()
 * @model
 * @generated
 */
public interface CreditReductionCourse extends EObject {
	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see WebPageCourses.WebPageCoursesPackage#getCreditReductionCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link WebPageCourses.CreditReductionCourse#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Credit Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credit Reduction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credit Reduction</em>' attribute.
	 * @see #setCreditReduction(double)
	 * @see WebPageCourses.WebPageCoursesPackage#getCreditReductionCourse_CreditReduction()
	 * @model
	 * @generated
	 */
	double getCreditReduction();

	/**
	 * Sets the value of the '{@link WebPageCourses.CreditReductionCourse#getCreditReduction <em>Credit Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credit Reduction</em>' attribute.
	 * @see #getCreditReduction()
	 * @generated
	 */
	void setCreditReduction(double value);

} // CreditReductionCourse
