# TDT4250 Advanced Software Engineering 

## WebPageCourse Description

### StudyProgram 
The root of the diagram, it can have many courses.

### Course
It has a code, name and content. Its value its in credits.

### CourseWork
Stores the information about the hours of the lectures and labs.

### RelatedCourses
Links the prerequisites to the course class.

### Requirement
Stores information about the required courses, can be recommended or mandatory.

### CreditReductionCourse
Stores information about the reductions of credits depending on the related courses.

### CourseInstance
Stores the semester information.

### Organization
The department in charge of the course. 

### People
Has a role (lecturer,coordinator or another) and a name.

### Timetable
Shows the timetable of the course.

### Schedule
Has the information about where and when the classes are taken.

### EvaluationForm
Stores information about the different types of work of the course.

### Work
Has a type (exam, assignments,work) and a percentage.

# Jorge Cristobal Martin